package hosam.octopart_test.adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hosam.octopart_test.R;
import hosam.octopart_test.models.Attribution;
import hosam.octopart_test.models.Description;
import hosam.octopart_test.models.Source;

public class DescriptionAdaptor extends RecyclerView.Adapter<DescriptionAdaptor.ViewHolder> {

    private ArrayList<Description> descriptions;

    public DescriptionAdaptor(ArrayList<Description> descriptions) {
        this.descriptions = descriptions;
    }

    @NonNull
    @Override
    public DescriptionAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.element_description, parent, false);

        DescriptionAdaptor.ViewHolder holder = new DescriptionAdaptor.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DescriptionAdaptor.ViewHolder holder, int position) {
        Attribution attribution = descriptions.get(position).getAttribution();
        holder.fillViewData(attribution);

    }

    @Override
    public int getItemCount() {
        return descriptions.size();
    }

    public void add(Description description) {
        descriptions.add(description);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(List<Description> descriptionList) {
        for (Description description : descriptionList) {
            add(description);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row

        public TextView tvDescription;
        public TextView tvProvider;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            super(itemView);
            tvDescription = itemView.findViewById(R.id.full_description);
            tvProvider = itemView.findViewById(R.id.description_provider);

        }

        public void fillViewData(Attribution attribution) {
            ArrayList<Source> sources = attribution.getSources();


            String sourcesString = "";

            if (!sources.isEmpty())
                sourcesString = sources.get(0).getName();


            tvProvider.setText(sourcesString);
            tvDescription.setText(descriptions.get(getAdapterPosition()).getValue());
        }
    }
}
