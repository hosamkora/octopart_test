package hosam.octopart_test.adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hosam.octopart_test.R;
import hosam.octopart_test.models.SpecValue;

public class PartSpecsAdaptor extends RecyclerView.Adapter<PartSpecsAdaptor.ViewHolder> {

    private ArrayList<SpecValue> specValues;

    public PartSpecsAdaptor(ArrayList<SpecValue> specValues) {
        this.specValues = specValues;
    }

    @NonNull
    @Override
    public PartSpecsAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.element_part_property, parent, false);

        PartSpecsAdaptor.ViewHolder holder = new PartSpecsAdaptor.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PartSpecsAdaptor.ViewHolder holder, int position) {
        SpecValue specValue = specValues.get(position);
        holder.fillViewData(specValue);

    }

    @Override
    public int getItemCount() {
        return specValues.size();
    }

    public void add(SpecValue specValue) {

        specValues.add(specValue);
        notifyItemInserted(getItemCount() - 1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row

        public TextView tvName;
        public TextView tvValue;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.property_name);
            tvValue = itemView.findViewById(R.id.property_value);

        }

        public void fillViewData(SpecValue specValue) {
            tvName.setText(specValue.getMetadata().getName());

            String value = "";
            if (specValue.getDisplay_value().isEmpty())
                for (String v : specValue.getValue())
                    value += v;
            else
                value = specValue.getDisplay_value();

            tvValue.setText(value);
        }
    }
}
