package hosam.octopart_test.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hosam.octopart_test.R;
import hosam.octopart_test.models.Asset;
import hosam.octopart_test.models.ImageSet;

public class ImageSetAdaptor extends RecyclerView.Adapter<ImageSetAdaptor.ViewHolder> {

    private ArrayList<ImageSet> imageSets;
    private Context context;

    public ImageSetAdaptor(ArrayList<ImageSet> imageSets) {
        this.imageSets = imageSets;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.element_image, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageSetAdaptor.ViewHolder holder, int position) {
        ImageSet imageSet = imageSets.get(position);
        holder.fillViewData(imageSet);

    }

    @Override
    public int getItemCount() {
        return imageSets.size();
    }

    public void add(ImageSet imageSet) {
        imageSets.add(imageSet);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(List<ImageSet> imageSets) {
        for (ImageSet imageSet : imageSets)
            add(imageSet);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row

        public TextView tvProvider;
        public ImageView ivProduct;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            super(itemView);
            ivProduct = itemView.findViewById(R.id.full_product_image);
            tvProvider = itemView.findViewById(R.id.image_provider);
        }

        public void fillViewData(ImageSet imageSet) {
            tvProvider.setText(imageSet.getCreditString());
            Asset image = imageSet.getValidImage();
            ImageView imageView = ivProduct;
            String imageURL = image.getUrl();

            if (!imageURL.isEmpty()) {
                Picasso.get().load(imageURL)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error_icon)
                        .into(imageView);

                imageView.setOnClickListener((v)->{
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(imageURL));
                        context.startActivity(i);
                });
            }
        }
    }
}
