package hosam.octopart_test.adaptors;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hosam.octopart_test.PartSpecsActivity;
import hosam.octopart_test.R;
import hosam.octopart_test.models.Part;


public class PartAdaptor extends RecyclerView.Adapter<PartAdaptor.ViewHolder> {
    private ArrayList<Part> parts;
    private Context mContext;
    private LoadMoreListener loadMoreListener;

    public PartAdaptor(ArrayList<Part> elements) {
        this.setParts(elements);

    }

    public PartAdaptor(ArrayList<Part> elements, Context context) {
        this(elements);
        this.setmContext(context);

    }

    @NonNull
    @Override
    public PartAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom category_back_stack_element
        View elementView = inflater.inflate(R.layout.element_list, parent, false);


        ViewHolder holder = new ViewHolder(elementView);

        // Return a new holder instance
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PartAdaptor.ViewHolder holder, int position) {

        // Get the data model based on position
        Part part = getParts().get(position);

        holder.fillViewData(part);


        if ((position >= getItemCount() - 1)) {
            getLoadMoreListener().loadMore(getItemCount());
        }

    }

    @Override
    public int getItemCount() {
        return getParts().size();
    }

    public void clear() {
        int size = getItemCount();
        getParts().clear();
        notifyItemRangeRemoved(0, size);
    }

    public void add(Part part) {
        getParts().add(part);
        notifyItemInserted(getItemCount() - 1);
    }

    public void setOnLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.setLoadMoreListener(loadMoreListener);
    }

    public ArrayList<Part> getParts() {
        return parts;
    }

    public void setParts(ArrayList<Part> parts) {
        this.parts = parts;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public LoadMoreListener getLoadMoreListener() {
        return loadMoreListener;
    }

    public void setLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public interface LoadMoreListener {
        void loadMore(int itemsCount);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row

        TextView tvName;
        TextView tvDescription;
        TextView tvBrand;
        ImageView ivProductImage;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.name);
            tvDescription = itemView.findViewById(R.id.description);
            tvBrand = itemView.findViewById(R.id.brand);
            ivProductImage = itemView.findViewById(R.id.product_image);
        }

        public void fillViewData(Part part) {
            int position = getAdapterPosition();
            itemView.setOnClickListener((view) -> {
                Intent intent = new Intent(getmContext(), PartSpecsActivity.class);
                intent.putExtra("uid", getParts().get(position).getUid());
                intent.putExtra("mpn" , part.getMpn());
                intent.putExtra("url" , part.getOctopart_url());
                getmContext().startActivity(intent);
                Log.e(String.valueOf(position) + " :", String.valueOf(getParts().get(position)));
            });

            // Get the data model based on position


            // Set item views based on your views and data model
            tvName.setText(part.getMpn());
            tvDescription.setText(part.getShort_description());
            tvBrand.setText(part.getBrand().getName());

            ImageView productView = ivProductImage;


            if (!part.getImageSets().isEmpty())
                if (!part.getImageSets().get(0).getSmallImage().getUrl().isEmpty())
                    Picasso.get().load(part.getImageSets().get(0).getSmallImage().getUrl())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_icon)
                            .into(productView);


        }

    }

}



