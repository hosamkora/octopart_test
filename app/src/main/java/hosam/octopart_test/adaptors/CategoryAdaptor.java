package hosam.octopart_test.adaptors;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hosam.octopart_test.R;
import hosam.octopart_test.SearchActivity;
import hosam.octopart_test.models.Category;

public class CategoryAdaptor extends RecyclerView.Adapter<CategoryAdaptor.ViewHolder> {

    private ArrayList<Category> categories;
    private CategoryCallBack categoryCallBack;
    private Context context;

    public CategoryAdaptor(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public void setCategoryCallBack(CategoryCallBack categoryCallBack) {
        this.categoryCallBack = categoryCallBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.element_category, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.fillViewData(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void add(Category category) {
        categories.add(category);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(List<Category> categories){
        int postion = getItemCount();
        categories.addAll(categories);
        notifyItemRangeInserted(postion , getItemCount()-1);
    }

    public void clearAll() {
        int lastPostion = getItemCount();
        categories.clear();
        notifyItemRangeRemoved(0, lastPostion);
        //notifyDataSetChanged();
    }

    public interface CategoryCallBack {
        //default void callCategory(String categoryUid){};
        void callCategory(String categoryUid, String categoryName);
        //void finsih(String categoryUid);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivCategoryImage, ivHasMore;
        private TextView tvCategoryName, tvCategoryHits;

        public ViewHolder(View itemView) {
            super(itemView);
            ivCategoryImage = itemView.findViewById(R.id.category_image);
            tvCategoryName = itemView.findViewById(R.id.category_name);
            tvCategoryHits = itemView.findViewById(R.id.category_hits);
            ivHasMore = itemView.findViewById(R.id.has_more);
            itemView.setOnClickListener(this);
        }

        void fillViewData(Category category) {
            tvCategoryName.setText(category.getName());
            tvCategoryHits.setText(String.valueOf(category.getNumParts()));

            if (!category.getImagesets().isEmpty())
                if (!category.getImagesets().get(0).getValidImage().getUrl().isEmpty())
                    Picasso.get().load(category.getImagesets().get(0).getValidImage().getUrl())
                            .resize(200, 200)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_icon)
                            .into(ivCategoryImage);

            if (category.getChildrenUids().isEmpty())
                ivHasMore.setVisibility(View.GONE);
            else
                ivHasMore.setVisibility(View.VISIBLE);
        }

        @Override
        public void onClick(View view) {
            int postion = getAdapterPosition();
            String uid = categories.get(postion).getUid();
            String name = categories.get(postion).getName();
            if (!categories.get(postion).getChildrenUids().isEmpty()) {
                clearAll();
                categoryCallBack.callCategory(uid, name);
            } else {
                Intent intent = new Intent(context, SearchActivity.class);
                intent.putExtra("query" , "");
                intent.putExtra("categoryUID", uid);
                intent.putExtra("categoryName" , name);
                context.startActivity(intent);
            }

        }
    }
}

