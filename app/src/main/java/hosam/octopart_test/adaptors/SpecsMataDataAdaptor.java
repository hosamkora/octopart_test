package hosam.octopart_test.adaptors;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;

import hosam.octopart_test.R;
import hosam.octopart_test.models.SpecMetadata;

public class SpecsMataDataAdaptor extends RecyclerView.Adapter<SpecsMataDataAdaptor.ViewHolder> {

    private ArrayList<SpecMetadata> metadataArrayList;
    private Context context;

    private AdapterCallBack callBack;

    public SpecsMataDataAdaptor(ArrayList<SpecMetadata> metadataArrayList) {
        this.metadataArrayList = metadataArrayList;

    }

    public void setCallBack(AdapterCallBack callBack) {
        this.callBack = callBack;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.element_filter, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SpecMetadata metadata = metadataArrayList.get(position);
        holder.bindData(metadata.getName(), metadata.getChecked());

        if (metadata.getChecked())
            holder.name.setTextColor(Color.BLUE);
        else
            holder.name.setTextColor(Color.BLACK);

        holder.checkBox.setOnClickListener(v -> {
            CheckBox checkBox = (CheckBox) v;
            boolean state = checkBox.isChecked();
            SpecMetadata metadata1 = metadataArrayList.get(holder.getAdapterPosition());

            String unit = null;
            if (metadata1.getUnit() != null)
                unit = metadata1.getUnit().getName();

            metadata1.setChecked(state);
            if (state) {
                holder.name.setTextColor(context.getResources().getColor(R.color.octopart_default));
                callBack.add(metadata1.getType(), valueWrapper(metadata.getKey()), metadata1.getName(), unit);
            } else {
                holder.name.setTextColor(Color.BLACK);
                callBack.remove(metadata1.getType(), valueWrapper(metadata.getKey()));
            }

        });


    }


    private String valueWrapper(String value) {
        return "specs." + value + ".value";
    }

    @Override
    public int getItemCount() {
        return metadataArrayList.size();
    }


    public void add(SpecMetadata metadata) {
        metadataArrayList.add(metadata);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(Collection<SpecMetadata> metadataiterator) {
        int start = getItemCount();
        int limit = metadataiterator.size();
        metadataArrayList.addAll(metadataiterator);
        notifyItemRangeInserted(start, limit);

    }

    @Override
    public int getItemViewType(int position) {
        return metadataArrayList.get(position).getType();
    }


    public interface AdapterCallBack {
        void add(int specType, String key, String name, String unit);

        void remove(int specType, String key);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.checkBox = itemView.findViewById(R.id.checkbox);
            itemView.setOnClickListener(this);

        }

        public void bindData(String name, boolean isChecked) {
            this.name.setText(name);
            this.checkBox.setChecked(isChecked);
        }

        @Override
        public void onClick(View view) {
            int index = getAdapterPosition();
            SpecMetadata metadata = metadataArrayList.get(index);
            if (metadata.getChecked()) {
                String key = metadata.getKey();
                int type = metadata.getType();

                String unit = null;
                if (metadata.getUnit() != null)
                    unit = metadata.getUnit().getName();

                callBack.add(type, valueWrapper(key), metadata.getName(), unit);
            }
        }
    }
}