package hosam.octopart_test.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hosam.octopart_test.R;
import hosam.octopart_test.models.Attribution;
import hosam.octopart_test.models.Datasheet;
import hosam.octopart_test.models.Source;

public class DatasheetAdaptor extends RecyclerView.Adapter<DatasheetAdaptor.ViewHolder> {
    private ArrayList<Datasheet> datasheets;
    private Context context;

    public DatasheetAdaptor(ArrayList<Datasheet> datasheets) {
        this.datasheets = datasheets;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.element_datasheet, parent, false);

        ViewHolder holder = new ViewHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Datasheet datasheet = datasheets.get(position);
        holder.fillViewData(datasheet);
    }

    @Override
    public int getItemCount() {
        return datasheets.size();
    }

    public void add(Datasheet datasheet) {
        datasheets.add(datasheet);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(List<Datasheet> datasheetList) {
        for (Datasheet datasheet : datasheetList)
            add(datasheet);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPdfProviderName;
        TextView tvNumOfPages;
        TextView tvPdfSize;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPdfProviderName = itemView.findViewById(R.id.pdf_provider_name);
            tvNumOfPages = itemView.findViewById(R.id.pdf_number_of_pages);
            tvPdfSize = itemView.findViewById(R.id.pdf_file_size);


        }

        public void fillViewData(Datasheet datasheet) {

            Attribution attribution = datasheet.getAttribution();
            Datasheet.Metadata metadata = datasheet.getMetaData();

            Source source = new Source();
            if (!attribution.getSources().isEmpty())
                source = attribution.getSources().get(0);

            tvNumOfPages.setText(metadata.getNumPages() + " Pages");
            tvPdfSize.setText(metadata.getSizeBytes() + " Bytes");

            tvPdfProviderName.setText(source.getName());


            itemView.setOnClickListener((v) -> {
                String url = datasheet.getUrl();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                context.startActivity(i);
            });
        }
    }
}
