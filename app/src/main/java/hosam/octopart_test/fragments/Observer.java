package hosam.octopart_test.fragments;

import hosam.octopart_test.models.Part;

public interface Observer {
    void showData(Part part);
}
