package hosam.octopart_test.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import hosam.octopart_test.R;
import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.adaptors.ImageSetAdaptor;
import hosam.octopart_test.models.ImageSet;
import hosam.octopart_test.models.Part;


/**
 * A simple {@link Fragment} subclass.
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class ImagesFragment extends Fragment implements Observer{

    ArrayList<ImageSet> imageSets;
    ImageSetAdaptor imageSetAdaptor;
    private String uid;

    View progressBar;

    public ImagesFragment() {
        // Required empty public constructor
    }

    public static ImagesFragment newInstance(String uid) {
        ImagesFragment fragment = new ImagesFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            this.uid = getArguments().getString("uid");

        imageSets = new ArrayList<>();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void showData(Part part) {
        ArrayList<ImageSet> imageSets = part.getImageSets();

        if (imageSets.isEmpty()){
            showEmptyData();
            return;
        }
        imageSetAdaptor.addAll(imageSets);
    }

    private void showEmptyData() {
        LayoutInflater inflater = LayoutInflater.from(this.getContext());
        View view = inflater.inflate(R.layout.no_data_found , null);
        TextView message = view.findViewById(R.id.message);
        message.setText("No Images Found");
        ((ViewGroup)this.getView()).addView(view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the category_back_stack_element for this fragment
        View view = inflater.inflate(R.layout.fragment_general, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list);

        imageSetAdaptor = new ImageSetAdaptor(imageSets);
        imageSetAdaptor.setContext(getActivity());

        recyclerView.setAdapter(imageSetAdaptor);


        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        return view;
    }




}