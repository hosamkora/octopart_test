package hosam.octopart_test.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdaptor extends FragmentStatePagerAdapter {
    String uid;

    public PagerAdaptor(FragmentManager fm, String uid) {
        super(fm);
        this.uid = uid;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SpecificationsFragment.newInstance(uid);
            case 1:
                return DescriptionsFragment.newInstance(uid);
            case 2:
                return ImagesFragment.newInstance(uid);
            case 3:
                return DatasheetsFragment.newInstance(uid);
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Specs";
            case 1:
                return "Descriptions";
            case 2:
                return "Images";
            case 3:
                return "Datasheets";
            default:
                return null;
        }
    }
}
