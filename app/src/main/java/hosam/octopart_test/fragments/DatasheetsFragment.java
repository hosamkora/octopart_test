package hosam.octopart_test.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hosam.octopart_test.R;
import hosam.octopart_test.adaptors.DatasheetAdaptor;
import hosam.octopart_test.models.Datasheet;
import hosam.octopart_test.models.Part;


/**
 * A simple {@link Fragment} subclass.
 */
public class DatasheetsFragment extends Fragment implements Observer {

    String uid;
    ArrayList<Datasheet> datasheets;
    DatasheetAdaptor datasheetAdaptor;

    public DatasheetsFragment() {
        // Required empty public constructor
    }

    public static DatasheetsFragment newInstance(String uid) {
        DatasheetsFragment fragment = new DatasheetsFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            this.uid = getArguments().getString("uid");

        datasheets = new ArrayList<>();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void showData(Part part) {
        ArrayList<Datasheet> datasheets = part.getDatasheets();
        if (datasheets.isEmpty()) {
            showEmptyData();
            return;
        }
        datasheetAdaptor.addAll(datasheets);
    }

    private void showEmptyData() {
        LayoutInflater inflater = LayoutInflater.from(this.getContext());
        View view = inflater.inflate(R.layout.no_data_found , null);
        TextView message = view.findViewById(R.id.message);
        message.setText("No Datasheets Found");
        ((ViewGroup)this.getView()).addView(view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the category_back_stack_element for this fragment
        View view = inflater.inflate(R.layout.fragment_general, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list);
        datasheetAdaptor = new DatasheetAdaptor(datasheets);
        datasheetAdaptor.setContext(getContext());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(datasheetAdaptor);

        return view;
    }



}



