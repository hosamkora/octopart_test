package hosam.octopart_test.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import hosam.octopart_test.R;
import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.adaptors.PartSpecsAdaptor;
import hosam.octopart_test.models.Part;
import hosam.octopart_test.models.SpecValue;


/**
 * A simple {@link Fragment} subclass.
 */
public class SpecificationsFragment extends Fragment implements Observer {

    ArrayList<SpecValue> specValues;
    PartSpecsAdaptor partSpecsAdaptor;
    private String uid;

    public SpecificationsFragment() {
        // Required empty public constructor
    }

    public static SpecificationsFragment newInstance(String uid) {
        SpecificationsFragment fragment = new SpecificationsFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            this.uid = getArguments().getString("uid");

        specValues = new ArrayList<>();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void showData(Part part) {
        HashMap<String, SpecValue> hashMap = part.getSpecs();
        Set<String> specKeys = hashMap.keySet();

        if (specKeys.isEmpty()){
            showEmptyData();
            return;
        }

        for (String key : specKeys) {
            SpecValue specValue = hashMap.get(key);
            int postion = partSpecsAdaptor.getItemCount();
            partSpecsAdaptor.add(specValue);
        }
    }

    private void showEmptyData() {
        LayoutInflater inflater = LayoutInflater.from(this.getContext());
        View view = inflater.inflate(R.layout.no_data_found , null);
        TextView message = view.findViewById(R.id.message);
        message.setText("No Specs Found");
        ((ViewGroup)this.getView()).addView(view);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the category_back_stack_element for this fragment
        View view = inflater.inflate(R.layout.fragment_general, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list);


        partSpecsAdaptor = new PartSpecsAdaptor(specValues);

        recyclerView.setAdapter(partSpecsAdaptor);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }




}
