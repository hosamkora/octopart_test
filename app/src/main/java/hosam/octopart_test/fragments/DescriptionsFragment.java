package hosam.octopart_test.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;

import hosam.octopart_test.R;
import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.adaptors.DescriptionAdaptor;
import hosam.octopart_test.models.Description;
import hosam.octopart_test.models.ImageSet;
import hosam.octopart_test.models.Part;

/**
 * A simple {@link Fragment} subclass.
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionsFragment extends Fragment implements Observer{

    ArrayList<Description> descriptions;
    DescriptionAdaptor descriptionAdaptor;
    Iterator<String> specs;
    private String uid;

    public DescriptionsFragment() {
        // Required empty public constructor
    }

    public static DescriptionsFragment newInstance(String uid) {
        DescriptionsFragment fragment = new DescriptionsFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            this.uid = getArguments().getString("uid");

        descriptions = new ArrayList<>();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void showData(Part part)
    {   ArrayList<Description> descriptions = part.getDescriptions();
        if (descriptions.isEmpty()) {
            showEmptyData();
            return;
        }
        descriptionAdaptor.addAll(descriptions);
    }

    private void showEmptyData() {
        LayoutInflater inflater = LayoutInflater.from(this.getContext());
        View view = inflater.inflate(R.layout.no_data_found , null);
        TextView message = view.findViewById(R.id.message);
        message.setText("No Descriptions Found");
        ((ViewGroup)this.getView()).addView(view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the category_back_stack_element for this fragment
        View view = inflater.inflate(R.layout.fragment_general, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list);


        descriptionAdaptor = new DescriptionAdaptor(descriptions);


        recyclerView.setAdapter(descriptionAdaptor);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }




}
