package hosam.octopart_test;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView t1 = findViewById(R.id.html_content);
        TextView t2 = findViewById(R.id.open_source);
        TextView t3 = findViewById(R.id.contact_us);

        renderHtmlInTextView(t1 , getResources().getString(R.string.about_content));
        renderHtmlInTextView(t2 ,getResources().getString(R.string.open_source));
        renderHtmlInTextView(t3 ,getResources().getString(R.string.contact_us));

        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = findViewById(R.id.ad);

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

    }

    void renderHtmlInTextView(TextView tv , String htmlContent){
        tv.setClickable(true);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(Html.fromHtml(htmlContent));
    }

    public void visitOctopart(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(getResources().getString(R.string.octopart_url)));
        this.startActivity(i);
    }
}
