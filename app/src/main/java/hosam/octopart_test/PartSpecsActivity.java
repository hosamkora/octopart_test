package hosam.octopart_test;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;

import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.fragments.Observer;
import hosam.octopart_test.fragments.PagerAdaptor;
import hosam.octopart_test.models.Part;
import hosam.octopart_test.services.OctopartClient;
import hosam.octopart_test.services.OctopartURLBuilder;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;


public class PartSpecsActivity extends AppCompatActivity {

    private String uid , url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part_specs);

        bindViews();

        TabLayout tabLayout = findViewById(R.id.tab_layout);


        ViewPager viewPager = findViewById(R.id.pager);

        uid = getIntent().getStringExtra("uid");
        url  = getIntent().getStringExtra("url");
        String mpn = getIntent().getStringExtra("mpn");

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(mpn);

        PagerAdaptor adaptor = new PagerAdaptor(getSupportFragmentManager(), uid);

        // stop fragment from reloading
        viewPager.setOffscreenPageLimit(4);


        viewPager.setAdapter(adaptor);
        tabLayout.setupWithViewPager(viewPager);

        placeCall();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.octopart_url, menu);
        return true;
    }



    public void goToPartWebPage(MenuItem item) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    void bindViews(){

        // activate ad
        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = findViewById(R.id.ad);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    void placeCall(){
        HttpUrl url =   OctopartURLBuilder
                .getItemsRequest(OctopartURLBuilder.RequestType.Parts, OctopartURLBuilder.EndPoint.UID)
                .setInclusions(
                        OctopartURLBuilder.Includes.DATASHEETS ,
                        OctopartURLBuilder.Includes.IMAGESETS ,
                        OctopartURLBuilder.Includes.SPECS ,
                        OctopartURLBuilder.Includes.Descriptions)
                .setShows(
                        OctopartURLBuilder.Includes.DATASHEETS ,
                        OctopartURLBuilder.Includes.IMAGESETS ,
                        OctopartURLBuilder.Includes.SPECS ,
                        OctopartURLBuilder.Includes.Descriptions)
                .buildByUID(uid);

        OkHttpClient client = OctopartClient.getClient();
        Request request = new Request.Builder().url(url).build();
        Call call = client.newCall(request);

        new CustomAsyncTask(this , jsonElement -> {
            Gson gson = new Gson();
            Part part = gson.fromJson(jsonElement,Part.class);
            FragmentManager manager = PartSpecsActivity.this.getSupportFragmentManager();
            for (Fragment fragment : manager.getFragments()){
                ((Observer) fragment).showData(part);
            }
        }).execute(call);

    }



}
