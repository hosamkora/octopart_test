package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Category {

    @SerializedName("uid")
    private
    String uid;

    @SerializedName("name")
    private
    String name;

    @SerializedName("parent_uid")
    private
    String parentUid;

    @SerializedName("children_uids")
    private
    ArrayList<String> childrenUids;

    @SerializedName("ancestor_uids")
    private
    ArrayList<String> ancestorUids;

    @SerializedName("ancestor_names")
    private
    ArrayList<String> ancestorNames;

    @SerializedName("num_parts")
    private
    int numParts;

    @SerializedName("imagesets")
    private
    ArrayList<ImageSet> imagesets;

    public String getUid() {
        return uid == null ? "" : uid;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public String getParentUid() {
        return parentUid == null ? "" : parentUid;
    }

    public ArrayList<String> getChildrenUids() {
        return childrenUids == null ? new ArrayList<String>() : childrenUids;
    }

    public ArrayList<String> getAncestorUids() {
        return ancestorUids == null ? new ArrayList<String>() : ancestorUids;
    }

    public ArrayList<String> getAncestorNames() {
        return ancestorNames == null ? new ArrayList<String>() : ancestorNames;
    }

    public int getNumParts() {
        return numParts;
    }

    public ArrayList<ImageSet> getImagesets() {
        return imagesets == null ? new ArrayList<ImageSet>() : imagesets;
    }
}
