package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class Description {
    @SerializedName("value")
    private String value;

    @SerializedName("attribution")
    private Attribution attribution;

    public Description() {
    }

    public String getValue() {
        return value == null ? "" : value;
    }

    public Attribution getAttribution() {
        return attribution == null ? new Attribution() : attribution;
    }

    @Override
    public String toString() {
        return "Description{" +
                "value='" + value + '\'' +
                ", attribution=" + attribution +
                '}';
    }
}
