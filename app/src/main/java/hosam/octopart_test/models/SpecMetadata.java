package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class SpecMetadata {

    public static final int VIEW_TYPE_Stats = 0;
    public static final int VIEW_TYPE_Facets = 1;

    @SerializedName("datatype")
    private String datatype;

    @SerializedName("key")
    private String key;

    @SerializedName("name")
    private String name;

    @SerializedName("unit")
    private UnitOfMeasurement unit;

    private Boolean checked = false;

    private int type;


    public SpecMetadata() {
    }

    public String getDatatype() {
        return datatype == null ? "" : datatype;
    }

    public String getKey() {
        return key == null ? "" : key;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public UnitOfMeasurement getUnit() {
        return unit == null ? new UnitOfMeasurement() : unit;
    }


    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public int getType() {
        return datatype.equals("string") ? VIEW_TYPE_Facets : VIEW_TYPE_Stats;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SpecMetadata{" +
                "datatype='" + datatype + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", unit=" + unit +
                '}';
    }
}
