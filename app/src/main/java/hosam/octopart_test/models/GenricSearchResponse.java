package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GenricSearchResponse {
    @SerializedName("facet_results")
    Fields fields;

    @SerializedName("hits")
    private int hits;

    @SerializedName("mesc")
    private int msec;

    @SerializedName("results")
    private ArrayList<GenricSearchResult> results;

    @SerializedName("spec_metadata")
    private HashMap<String, SpecMetadata> specMetadataHashMap;

    @SerializedName("stats_results")
    private HashMap<String, SearchStatsResult> statsResultHashMap;

    @SerializedName("request")
    private SearchRequest searchRequest;

    public GenricSearchResponse() {
    }

    public Fields getFields() {
        return fields == null ? new Fields() : fields;
    }

    public int getHits() {
        return hits;
    }

    public int getMsec() {
        return msec;
    }

    public ArrayList<GenricSearchResult> getResults() {
        return results == null ? new ArrayList<GenricSearchResult>() : results;
    }

    public HashMap<String, SpecMetadata> getSpecMetadataHashMap() {
        return specMetadataHashMap == null ? new HashMap<String, SpecMetadata>() : specMetadataHashMap;
    }

    public HashMap<String, SearchStatsResult> getStatsResultHashMap() {
        return statsResultHashMap == null ? new HashMap<String, SearchStatsResult>() : statsResultHashMap;
    }

    public SearchRequest getSearchRequest() {
        return searchRequest == null ? new SearchRequest() : searchRequest;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "hits=" + hits +
                ", msec=" + msec +
                ", results=" + results +
                ", specMetadataHashMap=" + specMetadataHashMap +
                ", statsResultHashMap=" + statsResultHashMap +
                '}';
    }

}
