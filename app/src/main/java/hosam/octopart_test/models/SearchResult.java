package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class SearchResult {
    @SerializedName("item")
    private Part item;

    @SerializedName("snippet")
    private String snippet;

    public SearchResult() {
    }

    public Part getItem() {
        return item == null ? new Part() : item;
    }

    public String getSnippet() {
        return snippet == null ? "" : snippet;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "item=" + item +
                ", snippet='" + snippet + '\'' +
                '}';
    }
}
