package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class UnitOfMeasurement {
    @SerializedName("name")
    private String name;

    @SerializedName("symbol")
    private String symbol;

    public UnitOfMeasurement() {
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public String getSymbol() {
        return symbol == null ? "" : symbol;
    }

    @Override
    public String toString() {
        return "UnitOfMeasurement{" +
                "name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}