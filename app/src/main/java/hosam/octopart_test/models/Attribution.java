package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Attribution {

    @SerializedName("sources")
    private ArrayList<Source> sources;

    public Attribution() {
    }

    public ArrayList<Source> getSources() {
        return sources == null ? new ArrayList<Source>() : sources;
    }

    @Override
    public String toString() {
        return "Attribution{" +
                "sources=" + sources +
                '}';
    }


}

