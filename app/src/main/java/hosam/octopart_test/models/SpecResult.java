package hosam.octopart_test.models;

public interface SpecResult {
    String buildFilter(String key);
}
