package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import hosam.octopart_test.UnitPrefix;

public class SearchStatsResult implements SpecResult {

    private double minFilter = 0;
    private double maxFilter = 0;

    private UnitPrefix unitPrefixMin = UnitPrefix.NONE;
    private UnitPrefix unitPrefixMax = UnitPrefix.NONE;

    private double maxN;
    private double minN;


    @SerializedName("max")
    private String maxS;

    @SerializedName("min")
    private String minS;

    @SerializedName("count")
    private int count;
    @SerializedName("missing")
    private int missing;


    public SearchStatsResult() {
    }

    public int getCount() {
        return count;
    }

    public double getMax() {
        try {
            maxN = Double.valueOf(maxS);
        } catch (NumberFormatException e) {
            double num = Double.valueOf(maxS.substring(0, maxS.indexOf('e')));
            double power = Double.valueOf(maxS.substring(maxS.indexOf('e') + 1));
            maxN = Math.pow(num, power);
        }
        return maxN;
    }

    public double getMin() {
        try {
            minN = Double.valueOf(minS);
        } catch (NumberFormatException e) {
            double num = Double.valueOf(minS.substring(0, minS.indexOf('e')));
            double power = Double.valueOf(minS.substring(minS.indexOf('e') + 1));
            minN = Math.pow(num, power);
        }

        return minN;
    }

    public int getMissing() {
        return missing;
    }

    public double getMaxFilter() {
        return maxFilter;
    }

    public void setMaxFilter(double maxFilter) {
        this.maxFilter = maxFilter;
    }

    public double getMinFilter() {
        return minFilter;
    }

    public void setMinFilter(double minFilter) {
        this.minFilter = minFilter;
    }

    public double getRealMaxFilter() {
        return maxFilter * unitPrefixMax.getValue();
    }

    public double getRealMinFilter() {
        return minFilter * unitPrefixMin.getValue();
    }

    public UnitPrefix getUnitPrefixMin() {
        return unitPrefixMin;
    }

    public void setUnitPrefixMin(UnitPrefix unitPrefixMin) {
        this.unitPrefixMin = unitPrefixMin;
    }

    public UnitPrefix getUnitPrefixMax() {
        return unitPrefixMax;
    }

    public void setUnitPrefixMax(UnitPrefix unitPrefixMax) {
        this.unitPrefixMax = unitPrefixMax;
    }

    @Override
    public String toString() {
        return "SearchStatsResult{" +
                "minFilter=" + minFilter +
                ", maxFilter=" + maxFilter +
                ", max='" + maxS + '\'' +
                ", min='" + minS + '\'' +
                '}';
    }

    @Override
    public String buildFilter(String key) {
        String filter = key + ":" + "[ " + getRealMinFilter() + " TO " + getRealMaxFilter() + " ]";
        return filter;
    }


}


