package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SpecValue {

    @SerializedName("metadata")
    Metadata metadata;
    @SerializedName("value")
    ArrayList<String> value;
    @SerializedName("display_value")
    private String display_value;

    public SpecValue() {
    }

    public String getDisplay_value() {
        return display_value == null ? "" : display_value;
    }

    public Metadata getMetadata() {
        return metadata == null ? new Metadata() : metadata;
    }

    public ArrayList<String> getValue() {
        return value == null ? new ArrayList<String>() : value;
    }

    @Override
    public String toString() {
        return "SpecValue{" +
                "display_value='" + display_value + '\'' +
                ", metadata=" + metadata +
                ", value=" + value +
                '}';
    }

    public class Metadata {

        @SerializedName("value")
        ArrayList<String> value;
        @SerializedName("name")
        private String name;
        @SerializedName("unit")
        private UnitOfMeasurement unit;

        public ArrayList<String> getValue() {
            return value == null ? new ArrayList<String>() : value;
        }

        public String getName() {
            return name == null ? "" : name;
        }

        public UnitOfMeasurement getUnit() {
            return unit == null ? new UnitOfMeasurement() : unit;
        }

        @Override
        public String toString() {
            return "Metadata{" +
                    "name='" + name + '\'' +
                    ", unit=" + unit +
                    '}';
        }
    }
}

