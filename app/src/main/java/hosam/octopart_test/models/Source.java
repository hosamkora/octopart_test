package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class Source {
    @SerializedName("name")
    String name;

    @SerializedName("uid")
    String uid;

    public Source() {
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public String getUid() {
        return uid == null ? "" : uid;
    }

    @Override
    public String toString() {
        return "Source{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }
}