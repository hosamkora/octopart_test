package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchFacetResult implements SpecResult {
    @SerializedName("facets")
    private ArrayList<Facet> facets;

    @SerializedName("missing")
    private String missing;

    public String filterValue;

    public int filterPostion;

    public ArrayList<Facet> getFacets() {
        return facets == null ? new ArrayList<Facet>() : facets;
    }

    public String getMissing() {
        return missing == null ? "" : missing;
    }

    public String getFilterValue() {
        return filterValue == null ? "" : filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public int getFilterPostion() {
        return filterPostion;
    }

    public void setFilterPostion(int filterPostion) {
        this.filterPostion = filterPostion;
    }


    @Override
    public String toString() {
        return "SearchFacetResult{" +
                ", filterValue='" + filterValue + '\'' +
                ", filterPostion=" + filterPostion +
                '}';
    }

    @Override
    public String buildFilter(String key) {
        String filter = key + ":" + filterValue;
        return filter;
    }

    public class Facet {
        @SerializedName("count")
        private long count;

        @SerializedName("value")
        private String value;

        public long getCount() {
            return count;
        }

        public String getValue() {
            return value == null ? "" : value;
        }

        @Override
        public String toString() {
            return "Facet{" +
                    "count='" + count + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}

