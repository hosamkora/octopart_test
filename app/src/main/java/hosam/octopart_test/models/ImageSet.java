package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class ImageSet {
    @SerializedName("credit_string")
    private String creditString;

    @SerializedName("small_image")
    private Asset smallImage;

    @SerializedName("medium_image")
    private Asset mediumImage;

    @SerializedName("large_image")
    private Asset largeImage;

    @SerializedName("swatch_image")
    private Asset swatchImage;

    public ImageSet() {
    }

    public String getCreditString() {
        return creditString == null ? "" : creditString;
    }

    public Asset getSmallImage() {
        return smallImage == null ? new Asset() : smallImage;
    }

    public Asset getMediumImage() {
        return mediumImage == null ? new Asset() : mediumImage;
    }

    public Asset getLargeImage() {
        return largeImage == null ? new Asset() : largeImage;
    }

    public Asset getSwatchImage() {
        return swatchImage == null ? new Asset() : swatchImage;
    }

    public Asset getValidImage() {
        if (!getLargeImage().getUrl().isEmpty())
            return largeImage;
        else if (!getMediumImage().getUrl().isEmpty())
            return mediumImage;
        else if (!getSmallImage().getUrl().isEmpty())
            return smallImage;
        else if (!getSwatchImage().getUrl().isEmpty())
            return swatchImage;

        return new Asset();

    }

    @Override
    public String toString() {
        return "ImageSet{" +
                "creditString='" + creditString + '\'' +
                ", smallImage=" + smallImage +
                ", mediumImage=" + mediumImage +
                ", largeImage=" + largeImage +
                ", swatchImage=" + swatchImage +
                '}';
    }
}

