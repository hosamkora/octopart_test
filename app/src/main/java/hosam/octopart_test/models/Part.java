package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class Part {


    @SerializedName("mpn")
    private String mpn;

    @SerializedName("uid")
    private String uid;

    @SerializedName("octopart_url")
    private String octopart_url;

    @SerializedName("short_description")
    private String short_description;

    @SerializedName("brand")
    private Brand brand;

    @SerializedName("specs")
    private HashMap<String, SpecValue> specs;

    @SerializedName("descriptions")
    private ArrayList<Description> descriptions;

    @SerializedName("imagesets")
    private ArrayList<ImageSet> imageSets;

    @SerializedName("datasheets")
    private ArrayList<Datasheet> datasheets;

    @SerializedName("spec_metadata")
    private HashMap<String, SpecMetadata> specMetadata;

    @SerializedName("stats_results")
    private HashMap<String, SearchStatsResult> statsResult;



    public Part() {
    }


    public String getMpn() {
        return mpn == null ? "" : mpn;
    }

    public String getUid() {
        return uid == null ? "" : uid;
    }

    public String getOctopart_url() {
        return octopart_url == null ? "" : octopart_url;
    }

    public String getShort_description() {
        return short_description == null ? "" : short_description;
    }

    public Brand getBrand() {
        return brand == null ? new Brand() : brand;
    }

    public HashMap<String, SpecValue> getSpecs() {
        return specs == null ? new HashMap<String, SpecValue>() : specs;
    }

    public ArrayList<Description> getDescriptions() {
        return descriptions == null ? new ArrayList<Description>() : descriptions;
    }

    public ArrayList<ImageSet> getImageSets() {
        return imageSets == null ? new ArrayList<ImageSet>() : imageSets;
    }

    public ArrayList<Datasheet> getDatasheets() {
        return datasheets == null ? new ArrayList<Datasheet>() : datasheets;
    }

    public HashMap<String, SpecMetadata> getSpecMetadata() {
        return specMetadata == null ? new HashMap<String, SpecMetadata>() : specMetadata;
    }

    public HashMap<String, SearchStatsResult> getStatsResult() {
        return statsResult == null ? new HashMap<String, SearchStatsResult>() : statsResult;
    }

    @Override
    public String toString() {
        return "Part{" +
                "mpn='" + mpn + '\'' +
                ", uid='" + uid + '\'' +
                ", octopart_url='" + octopart_url + '\'' +
                ", short_description='" + short_description + '\'' +
                ", brand=" + brand +
                ", specs=" + specs +
                ", descriptions=" + descriptions +
                ", imageSets=" + imageSets +
                ", datasheets=" + datasheets +
                ", specMetadata=" + specMetadata +
                ", statsResult=" + statsResult +
                '}';
    }
}
