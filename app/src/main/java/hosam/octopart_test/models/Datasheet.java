package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Datasheet {
    @SerializedName("attribution")
    private Attribution attribution;

    @SerializedName("sources")
    private ArrayList<Source> sources;

    @SerializedName("metadata")
    private Metadata metaData;

    @SerializedName("url")
    private String url;

    public Datasheet() {
    }

    public Attribution getAttribution() {
        return attribution == null ? new Attribution() : attribution;
    }

    public ArrayList<Source> getSources() {
        return sources == null ? new ArrayList<Source>() : sources;
    }

    public Metadata getMetaData() {
        return metaData == null ? new Metadata() : metaData;
    }

    public String getUrl() {
        return url == null ? "" : url;
    }

    @Override
    public String toString() {
        return "Datasheet{" +
                "attribution=" + attribution +
                ", sources=" + sources +
                ", metaData=" + metaData +
                ", url='" + url + '\'' +
                '}';
    }

    public class Metadata {
        @SerializedName("data_created")
        private String dataCreated;

        @SerializedName("last_updated")
        private String lastUpdated;

        @SerializedName("num_pages")
        private int numPages;

        @SerializedName("size_bytes")
        private int sizeBytes;

        public Metadata() {
        }

        public String getDataCreated() {
            return dataCreated == null ? "N/A" : dataCreated;
        }

        public String getLastUpdated() {
            return lastUpdated == null ? "N/A" : lastUpdated;
        }


        public int getNumPages() {
            return numPages;
        }

        public int getSizeBytes() {
            return sizeBytes;
        }

        @Override
        public String toString() {
            return "Metadata{" +
                    "dataCreated='" + dataCreated + '\'' +
                    ", lastUpdated='" + lastUpdated + '\'' +
                    ", numPages='" + numPages + '\'' +
                    ", sizeBytes='" + sizeBytes + '\'' +
                    '}';
        }
    }

}

