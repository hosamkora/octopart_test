package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class Asset {
    @SerializedName("mimetype")
    private String mimetype;

    @SerializedName("url")
    private String url;

    public Asset() {
    }

    public String getMimetype() {
        return mimetype == null ? "" : mimetype;
    }

    public String getUrl() {
        return url == null ? "" : url;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "mimetype='" + mimetype + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
