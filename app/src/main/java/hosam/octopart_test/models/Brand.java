package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("uid")
    private String uid;

    @SerializedName("name")
    private String name;

    public Brand() {
    }

    public String getUid() {
        return uid == null ? "" : uid;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
