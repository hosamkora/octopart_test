package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

public class SearchRequest {
    @SerializedName("limit")
    private int limit;

    @SerializedName("start")
    private int start;

    public SearchRequest() {
    }

    public int getLimit() {
        return limit;
    }

    public int getStart() {
        return start;
    }

    @Override
    public String toString() {
        return "SearchRequest{" +
                "limit=" + limit +
                ", start=" + start +
                '}';
    }
}
