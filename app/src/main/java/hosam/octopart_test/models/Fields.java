package hosam.octopart_test.models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class Fields {

    @SerializedName("fields")
    private HashMap<String, SearchFacetResult> fields;

    public HashMap<String, SearchFacetResult> getFields() {
        return fields == null ? new HashMap<String, SearchFacetResult>() : fields;
    }

    @Override
    public String toString() {
        return "Fields{" +
                "fields=" + fields +
                '}';
    }
}
