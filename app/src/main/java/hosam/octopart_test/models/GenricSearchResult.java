package hosam.octopart_test.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class GenricSearchResult {
    @SerializedName("item")
    private JsonObject item;

    @SerializedName("snippet")
    private String snippet;

    public GenricSearchResult() {
    }

    public JsonObject getItem() {
        return item;
    }

    public String getSnippet() {
        return snippet == null ? "" : snippet;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "item=" + item +
                ", snippet='" + snippet + '\'' +
                '}';
    }
}
