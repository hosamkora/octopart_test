package hosam.octopart_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;

import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.adaptors.SpecsMataDataAdaptor;
import hosam.octopart_test.models.SearchFacetResult;
import hosam.octopart_test.models.SearchResponse;
import hosam.octopart_test.models.SearchStatsResult;
import hosam.octopart_test.models.SpecMetadata;
import hosam.octopart_test.models.SpecResult;
import hosam.octopart_test.services.OctopartClient;
import hosam.octopart_test.services.OctopartURLBuilder;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class AllSpecsActivity extends AppCompatActivity implements SpecsMataDataAdaptor.AdapterCallBack {
    private SpecsMataDataAdaptor mataDataAdaptor;

    final CustomOnClickListner customOnClickListner = new CustomOnClickListner();

    String query = "";
    String categoryUid = "";


    private TextView tvHits, tvValueName, tvMinHint, tvMaxHint;
    private LinearLayout llGroupView, llHintsGroup;
    private Spinner spinner;
    private NumberPicker pickerMinUnit, pickerMaxUnit;
    private EditText etMin, etMax;
    private Button btnApply, btnDone, btnCancel;
    private RecyclerView recyclerView;
    private ArrayAdapter<String> facetSpinnerAdapter;

    private HashMap<String, SpecResult> filters;

    private HashMap<String, SearchFacetResult> facetResult;
    private HashMap<String, SearchStatsResult> statsResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_specs);

        // intiallize views variables
        bindViews();


        // intialize local variables
        query = getIntent().getStringExtra("query");
        int hits = getIntent().getIntExtra("hits", 0);
        categoryUid = getIntent().getStringExtra("categoryUid");

        if (hits == 0) {
            showEmptyData();
            return;
        }

        ArrayList<SpecMetadata> specMetadata = new ArrayList<>();
        ArrayList<String> spinnerList = new ArrayList<>();
        String[] prefixesStrings = UnitPrefix.getStrings();

        // intiallize fields
        filters = new HashMap<>();
        mataDataAdaptor = new SpecsMataDataAdaptor(specMetadata);
        facetSpinnerAdapter = new ArrayAdapter<String>(
                this
                , R.layout.support_simple_spinner_dropdown_item);

        // do all the other stuff

        pickerMinUnit.setMinValue(0);
        pickerMinUnit.setMaxValue(prefixesStrings.length - 1);
        pickerMinUnit.setDisplayedValues(prefixesStrings);
        pickerMinUnit.setWrapSelectorWheel(false);
        pickerMinUnit.setValue(6);


        pickerMaxUnit.setMinValue(0);
        pickerMaxUnit.setMaxValue(prefixesStrings.length - 1);
        pickerMaxUnit.setDisplayedValues(prefixesStrings);
        pickerMaxUnit.setWrapSelectorWheel(false);
        pickerMaxUnit.setValue(6);


        spinner.setAdapter(facetSpinnerAdapter);


        tvHits.setText(String.valueOf(hits));


        mataDataAdaptor.setContext(this);
        mataDataAdaptor.setCallBack(this);

        recyclerView.setAdapter(mataDataAdaptor);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        makeCall(query);

        // add listners to all required views
        addListners();

    }

    private void showEmptyData() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.no_data_found , null);
        TextView message = view.findViewById(R.id.message);
        message.setText("No Filters Found");
        ((ViewGroup)getWindow().getDecorView().getRootView()).addView(view);
    }

    private void addListners() {
        btnDone.setOnClickListener(v -> {

            Intent intent = new Intent();
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (!filters.isEmpty()) {
                ArrayList<String> filterList = new ArrayList<>();
                for (String key : filters.keySet())
                    filterList.add(filters.get(key).buildFilter(key));

                intent.putStringArrayListExtra("filters", filterList);
                setResult(1, intent);
            } else {
                Toast.makeText(getApplicationContext(), "No Filters Applied", Toast.LENGTH_SHORT).show();
                setResult(-1, intent);
            }
            finish();
        });

        btnApply.setOnClickListener(customOnClickListner);

        btnCancel.setOnClickListener((v) -> {
            tvValueName.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            btnApply.setVisibility(View.GONE);
            llGroupView.setVisibility(View.GONE);
            llHintsGroup.setVisibility(View.GONE);

            btnDone.setVisibility(View.VISIBLE);
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {

            }
        });
    }

    private void bindViews() {
        recyclerView = findViewById(R.id.list);
        spinner = findViewById(R.id.spinner);
        llGroupView = findViewById(R.id.view_group);
        etMin = findViewById(R.id.min);
        etMax = findViewById(R.id.max);
        tvValueName = findViewById(R.id.value_name);
        btnDone = findViewById(R.id.done);
        btnApply = findViewById(R.id.apply);
        btnCancel = findViewById(R.id.cancel);
        pickerMinUnit = findViewById(R.id.min_unit_picker);
        pickerMaxUnit = findViewById(R.id.max_unit_picker);
        tvHits = findViewById(R.id.hits);
        tvMinHint = findViewById(R.id.min_hint);
        tvMaxHint = findViewById(R.id.max_hint);
        llHintsGroup = findViewById(R.id.values_hint);

        // activate ad
        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = findViewById(R.id.ad);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void makeCall(String query) {
        OkHttpClient client = OctopartClient.getClient(20);

        OctopartURLBuilder urlBuilder = OctopartURLBuilder.getItemsRequest(
                OctopartURLBuilder.RequestType.Parts ,
                OctopartURLBuilder.EndPoint.Search);

        urlBuilder
                .setQuery(query)
                .setLimit(0)
                .setSpec_drilldown(true);


        ArrayList<String> filterList = new ArrayList<>();

        if (!categoryUid.isEmpty())
            urlBuilder.setFilters("category_uids:" + categoryUid);

        if (!filters.isEmpty()) {
            for (String key : filters.keySet())
                urlBuilder.setFilters(filters.get(key).buildFilter(key));
        }

        Request request = new Request.Builder().url(urlBuilder.build()).build();

        Call call = client.newCall(request);

        new CustomAsyncTask(this,jsonElement -> {
            Gson gson = new Gson();
            SearchResponse searchResponse = gson.fromJson(jsonElement , SearchResponse.class);
            int hits = searchResponse.getHits();
            tvHits.setText(String.valueOf(hits));

            HashMap<String, SpecMetadata> metadata = searchResponse.getSpecMetadataHashMap();


            ArrayList<SpecMetadata> arrayList = new ArrayList<>(metadata.values());

            facetResult = searchResponse.getFields().getFields();
            statsResult = searchResponse.getStatsResultHashMap();

            for (String key : metadata.keySet()) {
                SpecMetadata m = metadata.get(key);
                String k = "specs." + m.getKey() + ".value";
                if (m.getDatatype().equals("string")) {
                    if (facetResult.get(k).getFacets() == null) {
                        facetResult.remove(k);
                        arrayList.remove(m);
                        continue;
                    }
                    if (facetResult.get(k).getFacets().isEmpty()) {
                        facetResult.remove(k);
                        arrayList.remove(m);
                    }
                } else {
                    if (statsResult.get(k).getCount() == 0) {
                        statsResult.remove(k);
                        arrayList.remove(m);
                    }

                }
            }

            Collections.sort(arrayList, (a, b) -> {
                SpecMetadata aa = a;
                SpecMetadata bb = b;
                return aa.getName().compareToIgnoreCase(bb.getName());
            });


            mataDataAdaptor.addAll(arrayList);

        }).execute(call);

    }

    private void makeHitsCall(String query) {
        OkHttpClient client = OctopartClient.getClient(20);

        OctopartURLBuilder urlBuilder = OctopartURLBuilder.getItemsRequest(
                OctopartURLBuilder.RequestType.Parts ,
                OctopartURLBuilder.EndPoint.Search);

        urlBuilder
                .setQuery(query)
                .setLimit(0)
                .setSpec_drilldown(true);


        ArrayList<String> filterList = new ArrayList<>();

        if (!categoryUid.isEmpty())
            urlBuilder.setFilters("category_uids:" + categoryUid);

        if (!filters.isEmpty()) {
            for (String key : filters.keySet())
                urlBuilder.setFilters(filters.get(key).buildFilter(key));
        }

        Request request = new Request.Builder().url(urlBuilder.build()).build();

        okhttp3.Call call = client.newCall(request);

        new CustomAsyncTask(this,jsonElement -> {
            Gson gson = new Gson();
            SearchResponse searchResponse = gson.fromJson(jsonElement , SearchResponse.class);
            int hits = searchResponse.getHits();
            tvHits.setText(String.valueOf(hits));
        }).execute(call);

    }

    @Override
    public void add(int specType, String key, String name, String unit) {

        btnApply.setVisibility(View.VISIBLE);
        tvValueName.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);

        btnDone.setVisibility(View.GONE);


        if (unit.isEmpty())
            tvValueName.setText(name);
        else
            tvValueName.setText(name + " (" + unit + ")");

        switch (specType) {

            case SpecMetadata.VIEW_TYPE_Stats:
                llGroupView.setVisibility(View.VISIBLE);
                llHintsGroup.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.GONE);
                fillStatsData(key);
                break;

            case SpecMetadata.VIEW_TYPE_Facets:
                llGroupView.setVisibility(View.GONE);
                llHintsGroup.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                fillFacetsData(key);
                break;
        }
    }

    @Override
    public void remove(int specType, String key) {
        llGroupView.setVisibility(View.GONE);
        llHintsGroup.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);
        btnApply.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        tvValueName.setVisibility(View.GONE);

        btnDone.setVisibility(View.VISIBLE);


        filters.remove(key);
        makeHitsCall(query);

    }

    private void fillFacetsData(String key) {

        SearchFacetResult searchFacetResult = facetResult.get(key);
        customOnClickListner.setResult(searchFacetResult);
        ArrayList<SearchFacetResult.Facet> facets = searchFacetResult.getFacets();

        facetSpinnerAdapter.clear();

        for (SearchFacetResult.Facet f : facets)
            facetSpinnerAdapter.add(f.getValue());

        facetSpinnerAdapter.notifyDataSetChanged();
        filters.put(key, searchFacetResult);
        spinner.setSelection(searchFacetResult.getFilterPostion());
    }

    private void fillStatsData(String key) {

        SearchStatsResult searchStatsResult = statsResult.get(key);
        customOnClickListner.setResult(searchStatsResult);
        double max = searchStatsResult.getMaxFilter();
        double min = searchStatsResult.getMinFilter();

        etMin.setText(String.valueOf(min));
        etMax.setText(String.valueOf(max));

        pickerMaxUnit.setValue(searchStatsResult.getUnitPrefixMax().ordinal());
        pickerMinUnit.setValue(searchStatsResult.getUnitPrefixMin().ordinal());

        tvMinHint.setText(String.valueOf(searchStatsResult.getMin()));
        tvMaxHint.setText(String.valueOf(searchStatsResult.getMax()));

        filters.put(key, searchStatsResult);
    }


    class CustomOnClickListner implements View.OnClickListener {
        private SpecResult result;


        @Override
        public void onClick(View view) {

            boolean isFilterd = false;

            if (result instanceof SearchStatsResult)
                try {
                    isFilterd = setStatsFilterValues();
                    llGroupView.setVisibility(View.GONE);
                    llHintsGroup.setVisibility(View.GONE);
                } catch (Exception e) {
                    return;
                }
            else {
                isFilterd = setFacetFilterValues();
                spinner.setVisibility(View.GONE);
            }



            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {

            }

            if (isFilterd) {
                makeHitsCall(query);
            }

            tvValueName.setVisibility(View.GONE);
            btnApply.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            btnDone.setVisibility(View.VISIBLE);
        }

        private boolean setStatsFilterValues() {
            boolean isFilterd = false;
            SearchStatsResult statsResult = ((SearchStatsResult) result);
            try {
                String[] prefixesStrings = UnitPrefix.getStrings();

                double min = Double.valueOf(etMin.getText().toString());
                double max = Double.valueOf(etMax.getText().toString());
                int minUnitLoc = pickerMinUnit.getValue();
                int maxUnitLoc = pickerMaxUnit.getValue();

                UnitPrefix minUnit = UnitPrefix.values()[minUnitLoc];
                UnitPrefix maxUnit = UnitPrefix.values()[maxUnitLoc];

                boolean nonSense = min * minUnit.getValue() > max * maxUnit.getValue();
                if (nonSense) {
                    Toast.makeText(getApplicationContext(), "Min value is largger than Max Value", Toast.LENGTH_SHORT).show();
                    throw new InputMismatchException();
                }

                isFilterd = true;
                statsResult.setMinFilter(min);
                statsResult.setMaxFilter(max);

                statsResult.setUnitPrefixMin(minUnit);
                statsResult.setUnitPrefixMax(maxUnit);


            } catch (NumberFormatException e) {
                isFilterd = true;
                statsResult.setMinFilter(0);
                statsResult.setMaxFilter(0);
            }


            return isFilterd;
        }

        private boolean setFacetFilterValues() {
            SearchFacetResult facetResult = (SearchFacetResult) result;
            String selectedValue = (String) spinner.getSelectedItem();
            int selectedIndex = spinner.getSelectedItemPosition();
            facetResult.setFilterValue(selectedValue);
            facetResult.setFilterPostion(selectedIndex);

            return true;
        }

        public void setResult(SpecResult result) {
            this.result = result;
        }
    }


}



