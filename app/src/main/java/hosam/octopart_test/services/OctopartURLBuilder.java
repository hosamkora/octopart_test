package hosam.octopart_test.services;

import java.util.ArrayList;

import okhttp3.HttpUrl;




public class OctopartURLBuilder {

    private final static String apikey = "e8cb7e03";
//    public static String MATCH_REQUEST = "match";
//    public static String SEARCH_REQUEST = "search";
//    public static String UID_REQUEST = "uid";
//    public static String UIDs_REQUEST = "uids[]";
//    private static String baseURL = "https://www.octopart.com/api/v3/";

    HttpUrl.Builder urlBuilder = new HttpUrl.Builder();

    private boolean spec_drilldown = false;
    private int limit = 10;
    private int start = 0;
    private String query = "";
    private String uid = "";

    private ArrayList<String> filters = new ArrayList<>();
    private ArrayList<String> inclusions = new ArrayList<>();
    private ArrayList<String> shows = new ArrayList<>();

    private OctopartURLBuilder(){}

    private OctopartURLBuilder(RequestType type, EndPoint endpoint){
        urlBuilder.scheme("https")
                .host("www.octopart.com")
                .addEncodedPathSegments("api/v3/")
                .addPathSegment(type.name)
                .addQueryParameter("apikey" , apikey);

        if (endpoint != EndPoint.UID)
            urlBuilder.addPathSegment(endpoint.name);
    }

    public static OctopartURLBuilder getItemsRequest(RequestType type, EndPoint endpoint) {
        return new OctopartURLBuilder(type,endpoint);
    }


    public static OctopartURLBuilder getItemsRequest(RequestType type , String...uids) {
        OctopartURLBuilder builder = new OctopartURLBuilder(type,EndPoint.Get_Multi);
        for (String uid : uids)
            builder.urlBuilder.addQueryParameter("uid[]" , uid);
        return builder;
    }

    public OctopartURLBuilder setStart(int start) {
        this.start = start;
        return this;
    }

    public OctopartURLBuilder setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public OctopartURLBuilder setQuery(String query) {
        this.query = query;
        return this;
    }

    public OctopartURLBuilder setUID(String uid) {
        this.uid = uid;
        return this;
    }



    public OctopartURLBuilder setFilters(ArrayList<String> filters) {
        for (String s:
             filters) {
            this.filters.add(s);
        }
        return this;
    }

    public OctopartURLBuilder setFilters(String... filters) {
        for (String s :
                filters) {
            this.filters.add(s);
        }

        return this;
    }



    public OctopartURLBuilder setInclusions(ArrayList<String> inclusions) {
        for (String s :
                inclusions) {
            this.inclusions.add(s);
        }
        return this;
    }

    public OctopartURLBuilder setInclusions(String... inclusions) {
        for (String s :
                inclusions) {
            this.inclusions.add(s);
        }

        return this;
    }

    public OctopartURLBuilder setShows(ArrayList<String> shows) {
        for (String s :
                shows) {
            this.shows.add(s);
        }

        return this;
    }

    public OctopartURLBuilder setShows(String... shows) {
        for (String s :
                shows) {
            this.shows.add(s);
        }

        return this;
    }

    public OctopartURLBuilder setSpec_drilldown(boolean spec_drilldown) {
        this.spec_drilldown = spec_drilldown;
        return this;
    }

    public HttpUrl build() {

        urlBuilder
                .addQueryParameter("q" , query)
                .addQueryParameter("start" , String.valueOf(start))
                .addQueryParameter("limit", String.valueOf(limit));

        if (spec_drilldown == true)
            urlBuilder
                    .addQueryParameter("spec_drilldown[include]" ,"true")
                    .addQueryParameter("spec_drilldown[limit]" , "100");

        for (String s:inclusions)
            urlBuilder.addQueryParameter("include[]" , s);

        for (String s : shows)
            urlBuilder.addQueryParameter("show[]" , s);

        for (String s : filters)
            urlBuilder.addQueryParameter("filter[queries][]" , s);



        return urlBuilder.build();
    }

    public HttpUrl buildByUID(String uid) {

        urlBuilder.addPathSegment(uid);

        for (String s:inclusions)
            urlBuilder.addQueryParameter("include[]" , s);

        for (String s : shows)
            urlBuilder.addQueryParameter("show[]" , s);

        for (String s : filters)
            urlBuilder.addQueryParameter("filter[queries][]" , s);



        return urlBuilder.build();
    }


    public enum RequestType {
        Parts("parts"), Categories("categories");

        public String name;

        RequestType(String name) {
            this.name = name;
        }
    }

    public enum EndPoint {
        UID("{uid}"), Get_Multi("get_multi") , Search("search") , Match("match");

        public String name;

        EndPoint(String name) {
            this.name = name;
        }

        public EndPoint setUID(String uid) {
            UID.name = uid;
            return this;
        }
    }

    public interface Includes {
        String SHORT_DESCRIPTION = "short_description";
        String DATASHEETS = "datasheets";
        String COMPLIANCE_DOCUMENTS = "compliance_documents";
        String Descriptions = "descriptions";
        String IMAGESETS = "imagesets";
        String SPECS = "specs";
        String CATEGORIES_UIDs = "category_uids";
        String EXTERNAL_LINKS = "external_links";
        String REFERANCE_DESIGNS = "reference_designs";
        String CAD_MODELS = "cad_models";
    }

}

class FilterBuilder {
    public static String facetFilter(String specName, String value) {
        return "spec." + specName + ".value:" + value;
    }

    public static String statsFilter(String specName, int min, int max) {
        return "spec." + specName + ".value:" + "[ " + min + " TO " + max + " ]";
    }
}

