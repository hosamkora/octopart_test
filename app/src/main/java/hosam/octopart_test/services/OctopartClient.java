package hosam.octopart_test.services;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class OctopartClient {
    private static OkHttpClient client;

    private OctopartClient() {
    }

    public static OkHttpClient getClient() {
        if (client == null)
            client = new OkHttpClient();

        return client;
    }

    public static OkHttpClient getClient(int requestTimeout) {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();
        }


        return client;
    }


}
