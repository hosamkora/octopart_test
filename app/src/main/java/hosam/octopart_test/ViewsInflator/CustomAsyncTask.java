package hosam.octopart_test.ViewsInflator;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.lang.ref.WeakReference;


import okhttp3.Call;
import okhttp3.Response;


public  class  CustomAsyncTask extends AsyncTask<Call,Integer, JsonElement> {
    private WeakReference<Activity> mActivity;
    private FinishCallback mCallback;

    public CustomAsyncTask(Activity activity , FinishCallback callback) {
    mActivity = new WeakReference<>(activity);
    mCallback = callback;
    }

    @Override
    protected final JsonElement doInBackground(Call... calls) {
        JsonElement jsonElement = null;
        Activity activity = mActivity.get();
        try {
            Response response = calls[0].execute();
            jsonElement =  new JsonParser().parse(response.body().string());
        } catch (IOException e) {
            if (activity != null)
                Error.show(activity ,"No Internet Connection");
        }
        return  jsonElement;
    }

    @Override
    protected void onPreExecute() {
        Activity activity = mActivity.get();
        if (activity != null)
            Loading.show(activity);

    }

    @Override
    protected void onPostExecute(JsonElement jsonElement) {
        Activity activity = mActivity.get();
        if (activity != null)
            Loading.hide(activity);

        if (jsonElement != null)
            mCallback.doWhenFinish(jsonElement);

    }


}
