package hosam.octopart_test.ViewsInflator;


import com.google.gson.JsonElement;

public interface FinishCallback {
    void doWhenFinish(JsonElement jsonElement);
}
