package hosam.octopart_test.ViewsInflator;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.lang.ref.WeakReference;

import hosam.octopart_test.R;

public class Loading {
    private static View view;
    public static void show(Activity activity){
        ViewGroup root = (ViewGroup) activity.getWindow().getDecorView().getRootView();

        if (view != null &&  root.indexOfChild(view) != -1)
           return;

                LayoutInflater inflater = activity.getLayoutInflater();
                view = inflater.inflate(R.layout.layout_loading, null);
                root.addView(view);
    }

    public static void hide(Activity activity){
        ViewGroup root = (ViewGroup) activity.getWindow().getDecorView().getRootView();

        if (view != null &&  root.indexOfChild(view) == -1)
            return;

        root.removeView(view);
    }
}
