package hosam.octopart_test;

import static java.lang.Math.pow;

public enum UnitPrefix {
    Tera(pow(10, 12), "Tera", "T"),
    GIGA(pow(10, 9), "giga", "G"),
    MEGA(pow(10, 6), "mega", "M"),
    KILO(pow(10, 3), "kilo", "k"),
    HECTO(pow(10, 2), "hecto", "h"),
    DECA(pow(10, 1), "deca", "da"),
    NONE(pow(10, 0), "none", ""),
    DECI(pow(10, -1), "deci", "d"),
    CENTI(pow(10, -2), "centi", "c"),
    MILLI(pow(10, -3), "milli", "m"),
    MICRO(pow(10, -6), "micro", "μ"),
    NANO(pow(10, -9), "nano", "n"),
    PICO(pow(10, -12), "pico", "p");


    private static String[] strings;
    private double value;
    private String name;
    private String abbreviation;

    UnitPrefix(double value, String name, String abbreviation) {
        this.value = value;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public static String[] getStrings() {
        if (strings == null) {
            strings = new String[UnitPrefix.values().length];
            for (UnitPrefix unitPrefix : UnitPrefix.values())
                strings[unitPrefix.ordinal()] = unitPrefix.getName();
        }

        return strings;
    }

    public double getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public String toString() {
        return name;
    }

}