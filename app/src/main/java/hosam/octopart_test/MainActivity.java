package hosam.octopart_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Stack;

import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.adaptors.CategoryAdaptor;
import hosam.octopart_test.models.Category;
import hosam.octopart_test.models.GenricSearchResponse;
import hosam.octopart_test.models.GenricSearchResult;
import hosam.octopart_test.services.OctopartClient;
import hosam.octopart_test.services.OctopartURLBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class MainActivity extends AppCompatActivity implements CategoryAdaptor.CategoryCallBack {


    private SearchView svSearchQuery;
    private RecyclerView rvCategoriesList;
    private TextView tvCategoriesStack;
    private ImageButton btnResetCategories;
    private String query = "";

    ///////////////

    static final private String parentUid = "8a1e4714bb3951d9";
    static final private String parentName = "Electronic Parts";

    private CategoryAdaptor categoryAdaptor;

    private Stack<String> uidStack = new Stack<>();
    private Stack<String> categoriesNames = new Stack<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bindViews();
        ///

        ArrayList<Category> categories = new ArrayList<>();
        categoryAdaptor = new CategoryAdaptor(categories);
        categoryAdaptor.setCategoryCallBack(this);

        rvCategoriesList.setAdapter(categoryAdaptor);
        rvCategoriesList.setLayoutManager(new LinearLayoutManager(this));


        callCategory(parentUid, parentName);
        //
        addListners();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.about, menu);
        return true;
    }

    public void showAbout(MenuItem item) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void addListners() {
        svSearchQuery.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("query", query);

                if (!uidStack.isEmpty())
                    intent.putExtra("categoryUID", uidStack.peek().equals(parentUid) ? "" : uidStack.peek());
                intent.putExtra("categoryName", categoriesNames.peek());
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        btnResetCategories.setOnClickListener((v) -> {
            categoryAdaptor.clearAll();
            uidStack.clear();
            categoriesNames.clear();


            callCategory(parentUid, parentName);
        });
    }

    private void bindViews() {
        svSearchQuery = findViewById(R.id.searchQuery);
        rvCategoriesList = findViewById(R.id.list);
        tvCategoriesStack = findViewById(R.id.categories_stack);
        btnResetCategories = findViewById(R.id.rest_categories);


        // activate ad
        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = findViewById(R.id.ad);
        mAdView.setAdListener(new Ad());
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);


    }


    void placeCall(String parentUid) {

        OctopartURLBuilder urlBuilder = OctopartURLBuilder.getItemsRequest(
                OctopartURLBuilder.RequestType.Categories,
                OctopartURLBuilder.EndPoint.Search);

        urlBuilder
                .setFilters("parent_uid:" + parentUid)
                .setInclusions(OctopartURLBuilder.Includes.IMAGESETS)
                .setLimit(100);

        OkHttpClient client = OctopartClient.getClient();
        Request request = new Request.Builder().url(urlBuilder.build()).build();

        okhttp3.Call call = client.newCall(request);

        new CustomAsyncTask(this, jsonElement -> {
            Gson gson = new Gson();
            GenricSearchResponse searchResponse = gson.fromJson(jsonElement, GenricSearchResponse.class);

            for (GenricSearchResult genricSearchResult : searchResponse.getResults()) {
                Category category = gson.fromJson(genricSearchResult.getItem(), Category.class);
                categoryAdaptor.add(category);
            }
        }).execute(call);

    }

    public void callCategory(String categoryUid, String name) {
        uidStack.add(categoryUid);
        categoriesNames.add(name);
        StringBuilder builder = new StringBuilder();

        for (int i = 0 ; i < categoriesNames.size() ; i++)
            if (i == 0)
                builder.append(categoriesNames.get(i));
            else
                builder.append("->").append(categoriesNames.get(i));


        tvCategoriesStack.setText(builder.toString());
        placeCall(categoryUid);

    }

    class Ad extends AdListener {
        @Override
        public void onAdClosed() {
            super.onAdClosed();
            Toast.makeText(MainActivity.this , "Closed" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdFailedToLoad(int i) {
            super.onAdFailedToLoad(i);
            Toast.makeText(MainActivity.this , "FailedToLoad" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdLeftApplication() {
            super.onAdLeftApplication();
            Toast.makeText(MainActivity.this , "LeftApplication" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdOpened() {
            super.onAdOpened();
            Toast.makeText(MainActivity.this , "Opened" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdLoaded() {
            super.onAdLoaded();
            Toast.makeText(MainActivity.this , "Loaded" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdClicked() {
            super.onAdClicked();
            Toast.makeText(MainActivity.this , "Clicked" , Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAdImpression() {
            super.onAdImpression();
            Toast.makeText(MainActivity.this , "Impression" , Toast.LENGTH_LONG).show();
        }
    }

}
