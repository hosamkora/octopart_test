package hosam.octopart_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;

import hosam.octopart_test.ViewsInflator.CustomAsyncTask;
import hosam.octopart_test.ViewsInflator.Error;
import hosam.octopart_test.adaptors.PartAdaptor;
import hosam.octopart_test.models.GenricSearchResponse;
import hosam.octopart_test.models.GenricSearchResult;
import hosam.octopart_test.models.Part;
import hosam.octopart_test.services.OctopartClient;
import hosam.octopart_test.services.OctopartURLBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;


public class SearchActivity extends AppCompatActivity {


    private ArrayList<Part> parts;
    private PartAdaptor adaptor;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private FloatingActionButton floatingActionButton;
    private Toolbar toolbar;

    TextView currentSearch , currentCategory;

    private int limit = 20;
    private int start = 0;
    private int hits = 0;
    private int pages = 0;
    private String query = "";
    private Boolean isLoading;
    private String categoryUid = "";
    private String categoryName = "";

    private ArrayList<String> filters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        bindViews();

        filters = new ArrayList<>();

        categoryUid = getIntent().getStringExtra("categoryUID");
        query = getIntent().getStringExtra("query");

        String name = getIntent().getStringExtra("categoryName");
        categoryName = name == null ? "":name;

        currentSearch.setText("Search: " + query);
        currentCategory.setText("Category: "+categoryName);



        parts = new ArrayList<>();
        adaptor = new PartAdaptor(parts, this);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setAdapter(adaptor);
        recyclerView.setLayoutManager(layoutManager);


        addListeners();

        placeCall();
    }

    private void addListeners() {
        adaptor.setOnLoadMoreListener((count) -> {

            if (!isLoading) {
                if (((start / limit) + 1) <= pages) {
                    start += limit;
                    placeCall();
                } else {
                    Toast.makeText(SearchActivity.this, "No More Data", Toast.LENGTH_LONG).show();
                }
            }
        });

        floatingActionButton.setOnClickListener((v) -> {
            Intent intent = new Intent(SearchActivity.this, AllSpecsActivity.class);
            intent.putExtra("query", query);
            intent.putExtra("hits", hits);
            intent.putExtra("categoryUid", categoryUid);
            startActivityForResult(intent, 0);
        });

    }

    private void bindViews() {
        recyclerView = findViewById(R.id.list);
        floatingActionButton = findViewById(R.id.filter);
        toolbar = findViewById(R.id.my_toolbar);
        currentSearch = findViewById(R.id.currentSearch);
        currentCategory = findViewById(R.id.currentCategory);

        // add my custom
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);



        // activate ad
        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = findViewById(R.id.ad);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void placeCall(){
        isLoading = true;
        OctopartURLBuilder builder =   OctopartURLBuilder
                .getItemsRequest(OctopartURLBuilder.RequestType.Parts, OctopartURLBuilder.EndPoint.Search)
                .setQuery(query)
                .setStart(start)
                .setLimit(20)
                .setFilters(filters)
                .setInclusions(
                        OctopartURLBuilder.Includes.SHORT_DESCRIPTION ,
                        OctopartURLBuilder.Includes.IMAGESETS)
                .setShows(
                        OctopartURLBuilder.Includes.SHORT_DESCRIPTION ,
                        OctopartURLBuilder.Includes.IMAGESETS,
                        "brand.name" ,
                        "mpn" ,
                        "uid" ,
                        "octopart_url");

        if ( categoryUid != null && !categoryUid.isEmpty())
            builder.setFilters("category_uids:" + categoryUid);

        OkHttpClient client = OctopartClient.getClient();
        Request request = new Request.Builder().url(builder.build()).build();
        okhttp3.Call call = client.newCall(request);

        new CustomAsyncTask(this , jsonElement -> {
            Gson gson = new Gson();
            GenricSearchResponse searchResponse = gson.fromJson(jsonElement,GenricSearchResponse.class);
            int h = searchResponse.getHits();
            if (h != 0) {
                isLoading = false;
                limit = searchResponse.getSearchRequest().getLimit();
                hits = h;
                start = searchResponse.getSearchRequest().getStart();
                pages = (hits / limit) + 1;
                ArrayList<GenricSearchResult> results = searchResponse.getResults();

                for (GenricSearchResult result : results) {
                    Part part  = gson.fromJson(result.getItem() , Part.class);
                    adaptor.add(part);
                }
            }
            else if(hits ==0 ){
                Error.show(SearchActivity.this , "No Parts Match Your Search : "+query);

            }
        }).execute(call);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view_menu_item, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();

        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String q) {
                searchViewAndroidActionBar.clearFocus();

                adaptor.clear();
                filters.clear();
                query = q;
                currentSearch.setText("Search: "+q);
                start = 0;
                hits = 0;


                isLoading = true;
                placeCall();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1) {
            filters = data.getStringArrayListExtra("filters");
            adaptor.clear();
            adaptor.notifyDataSetChanged();

            start = 0;
            placeCall();
        }
    }


}

