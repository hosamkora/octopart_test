<p float="left" style="overflow-x: scroll;">
  <img src="https://image.winudf.com/v2/image/aG9zYW0ub2N0b3BhcnRfdGVzdF9zY3JlZW5fMF8xNTM2OTU1MDY2XzA3Ng/screen-0.jpg?h=355&fakeurl=1&type=.webp" width="100" />
  <img src="https://image.winudf.com/v2/image/aG9zYW0ub2N0b3BhcnRfdGVzdF9zY3JlZW5fMV8xNTM2OTU1MDY3XzA2OA/screen-1.jpg?h=355&fakeurl=1&type=.webp" width="100" /> 
  <img src="https://image.winudf.com/v2/image/aG9zYW0ub2N0b3BhcnRfdGVzdF9zY3JlZW5fM18xNTM2OTU1MDY5XzA1OA/screen-3.jpg?h=355&fakeurl=1&type=.webp" width="100" /> 
  <img src="https://image.winudf.com/v2/image/aG9zYW0ub2N0b3BhcnRfdGVzdF9zY3JlZW5fNF8xNTM2OTU1MDcwXzAwOQ/screen-4.jpg?h=355&fakeurl=1&type=.webp" width="100" /> 
  <img src="https://image.winudf.com/v2/image/aG9zYW0ub2N0b3BhcnRfdGVzdF9zY3JlZW5fNl8xNTM2OTU1MDczXzAwOA/screen-6.jpg?h=355&fakeurl=1&type=.webp" width="100" /> 
</p>
# Electronic Components Finder is Powered By Octopart API . [Apk Pure Store](https://m.apkpure.com/electronic-components-finder/hosam.octopart_test)

Electronic Components Finder is an online search engine for electronic components which categorized to 14 main categories :  
▪ Integrated Circuits (ICs) ▪ Discrete Semiconductors  
▪ Passive Components ▪ Electromechanical  
▪ Connectors ▪ Test Equipment  
▪ Tools and Supplies ▪ Sensors  
▪ Optoelectronics ▪ Circuit Protection  
▪ Power Products ▪ Cables and Wire  
▪ Machining ▪ Industrial Control  

Each Component in the result of your search has:  
▸ Specifications  
▸ Images  
▸ Descriptions  
▸ Datasheets  

And You can filter your search by :  
▸ Specifications ( bandwidth , capacitance , resistance , input current , .... etc )  
▸ Category  

If you want to suggest new feature or to report some bugs , Don't hesitate to contact me at this email "hosamkora@gmail.com"